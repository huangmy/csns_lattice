#!/usr/bin/env pyORBIT
import time
import orbit_mpi
import argparse
from lib.timecalc import timecalc
from lib import utils
from orbit.time_dep import time_dep_csns
from lattice_builder import csns_builder


class Tracker:

    def __init__(self, configfile = None):
        with open(configfile, 'r') as f_in:
            self.builder = csns_builder(f_in)
        self.remain = 0
        self.tracking_time = 0
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        self.rank = orbit_mpi.MPI_Comm_rank(comm)

    def begin(self):
        ##########################################
        #Begin tracking
        ##########################################
        lattice = self.builder.get_lattice()
        paramsDict = self.builder.get_bunches()
        b = paramsDict["bunch"]
        track = self.builder.get_track_setting()
        starttime = time.time()
        trackturn = track['turn']
        info = track['info']
        if isinstance(lattice,time_dep_csns.TIME_DEP_Lattice):
            lattice.setShowInfo(info)
            lattice.setTurns(trackturn) # Tracking for AC mode 
            lattice.trackBunchTurns(b, paramsDict)
        else:
            for i in xrange(trackturn):
                lattice.trackBunch(b, paramsDict) # Tracking for DC mode
                #bsize=b.getSizeGlobal()

        endtime = time.time()
        b.compress()
        self.remain=b.getSizeGlobal()
        cst = endtime-starttime
        self.tracking_time = '{:02}:{:02}:{:02}'.format(int(cst) / 3600,
                int(cst) / 60 % 60, cst % 60)
        #self.tracking_time = endtime-starttime
    def __del__(self):
        '''Destructor'''
        active_diag = self.builder.get_active_diag()
        for fil in active_diag:
            fil.closeFile()
        if self.rank==0:
            print "Total tracking time %s" % (self.tracking_time)
            print "Remain particle size %d" % (self.remain)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", type=str, help="The config file!")
    args = parser.parse_args()
    tracker = Tracker(args.configfile)
    tracker.begin()
    #try:
    #    tracker.begin()
    #except Exception as e:
    #    raise e
