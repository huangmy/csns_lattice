'''
Add aperture nodes into the lattice
@author Xiaohan Lu
@email luxh@ihep.ac.cn
@time 2018-10-28
'''
import copy
from orbit.aperture import EllipseApertureNode,CircleApertureNode
from orbit.aperture import addTeapotApertureNode


def add_CSNS_RCS_Aperture(lattice, machine):
    '''
    we don't assign the position while defining the aperture node
    the position will be assigned when we add the aperture node into the lattice,
    '''

    posDict = lattice.getNodePositionsDict()

    BV = lattice.getNodesForName('BV')
    for f in BV:
        #radius,pos,offsetx,offsety
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BV02":
            after = CircleApertureNode(0.163/2,0,0,-0.0143)
        if name == "BV03":
            after = CircleApertureNode(0.150/2,0,0.006,-0.008)
        addAperture(posDict,f,after=after)

    BH = lattice.getNodesForName('BH')
    for f in BH:
        name = f.getName()
        after = CircleApertureNode(0.163/2,0,0,0)
        if name == "BH02":
            after = CircleApertureNode(0.163/2,0,0,-0.0143)
        if name == "BH03":
            after = CircleApertureNode(0.150/2,0,0.006,-0.008)
        addAperture(posDict,f,after=after)

    BC = lattice.getNodesForName('BC')
    for f in BC:
        name = f.getName()
        before = CircleApertureNode(0.152/2,0,0.0385,-0.014)
        after = CircleApertureNode(0.152/2,0,0.0385,-0.014)
        mid = CircleApertureNode(0.152/2,0,0.0385,-0.014)
        if name == "BC01":
            before = CircleApertureNode(0.152/2,0,0,-0.014)
            after = CircleApertureNode(0.152/2,0,0,-0.014)
            mid = CircleApertureNode(0.152/2,0,0,-0.014)
        addAperture(posDict,f,before,after, mid)

    R4DH01_aperture = CircleApertureNode(0.220/2,0,0,0)
    R1DH01_aperture = CircleApertureNode(0.190/2,0,0,0)

    #R1CAV02,R3CAV01,R3CAV02,R3CAV03,R4CAV01,R4CAV02,R4CAV03
    R1CAV01_aperture = CircleApertureNode(0.170/2,0,0,0)
    R2CAV_D_aperture = CircleApertureNode(0.250/2,0,0,0)#R3CAV_D,R4CAV_D

    #COLM_aperture = CircleApertureNode(0.250/2,0,0,0)
    #COLP_aperture = CircleApertureNode(0.280/2,0,0,0)
    #COLS1_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS2_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS3_aperture = CircleApertureNode(0.228/2,0,0,0)
    #COLS4_aperture = CircleApertureNode(0.228/2,0,0,0)

##########################################################################
#put the aperture node at the entry, middle and exit of the associated node.    
#########################################################################
    QF01 = lattice.getNodesForName('QF01')
    QD02 = lattice.getNodesForName('QD02')
    QF03 = lattice.getNodesForName('QF03')
    QF04 = lattice.getNodesForName('QF04')
    QF06 = lattice.getNodesForName('QF06')
    MB = lattice.getNodesForName('bend')

    for f in MB:
        before = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
        after = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
        mid = EllipseApertureNode(0.218/2, 0.135/2, 0, 0, 0)
        addAperture(posDict,f,before,after, mid)

    for f in QF01:
        before = CircleApertureNode(0.183/2,0,0,0)
        after = CircleApertureNode(0.183/2,0,0,0)
        mid = CircleApertureNode(0.183/2,0,0,0)
        addAperture(posDict,f,before,after, mid)

    for f in QD02:
        before = CircleApertureNode(0.248/2,0,0,0)
        after = CircleApertureNode(0.248/2,0,0,0)
        mid = CircleApertureNode(0.248/2,0,0,0)
        #if f.getParam('subname') == 'R1QD02':# this one is special
        #    before = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
        #    after = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
        #    mid = EllipseApertureNode(0.218/2,0.135/2,0,0,0)
        addAperture(posDict,f,before,after, mid)

    for f in QF03:
        before = CircleApertureNode(0.183/2,0,0,0)
        after = CircleApertureNode(0.183/2,0,0,0)
        mid = CircleApertureNode(0.183/2,0,0,0)
        addAperture(posDict,f,before,after, mid)

    for f in QF04:
        before = CircleApertureNode(0.199/2,0,0,0)
        after = CircleApertureNode(0.199/2,0,0,0)
        mid = CircleApertureNode(0.199/2,0,0,0)
        addAperture(posDict,f,before,after, mid)

    for f in QF06:
        before = CircleApertureNode(0.230/2,0,0,0)
        after = CircleApertureNode(0.230/2,0,0,0)
        mid = CircleApertureNode(0.230/2,0,0,0)
        addAperture(posDict,f,before,after, mid)

    #add four special vacumm pipes in the lattice
    R1QD02_pipe = CircleApertureNode(0.1, 0, 0, 0)
    R2QD11_pipe = CircleApertureNode(0.1, 0, 0, 0)
    R3QD02_pipe = CircleApertureNode(0.1, 0, 0, 0)
    R4QD11_pipe = CircleApertureNode(0.1, 0, 0, 0)
    addTeapotApertureNode(lattice, 7.81, R1QD02_pipe)
    addTeapotApertureNode(lattice, 64.79, R2QD11_pipe)
    addTeapotApertureNode(lattice, 121.77, R3QD02_pipe)
    addTeapotApertureNode(lattice, 178.75, R4QD11_pipe)

    lattice.initialize()

#add aperture to a node
def addAperture(posDict,node,before=None,after=None,mid=None):
    posArray = posDict[node]
    if before:
        before.setPosition(posArray[0])
        node.addChildNode(before, 0)
    if after:
        after.setPosition(posArray[1])
        node.addChildNode(after, 2)
    if mid:
        mid.setPosition((posArray[0]+posArray[1])/2)
        node.addChildNode(mid, 1, 1)
