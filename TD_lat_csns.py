############################################################
#This is the time-dependent (AC mode) lattice of CSNS/RCS
#@Author: Xiaohan Lu
#@Email: luxh@ihep.ac.cn
#@Copyright (C) 2018 China Spallation Neutron Source (CSNS) 
###########################################################
import copy
from math import *
from bunch import Bunch
from orbit.teapot import teapot
from orbit.rf_cavities import RFNode, RFLatticeModifications
from orbit.collimation import TeapotCollimatorNode, addTeapotCollimatorNode
from orbit.kickernodes import addTeapotKickerNode, TeapotXKickerNode, TeapotYKickerNode, RangeRootTWaveform
from orbit.space_charge.sc2p5d import setSC2p5DAccNodes
from spacecharge import SpaceChargeCalc2p5D
from spacecharge import Boundary2D
from aperture_rcs import add_CSNS_RCS_Aperture
from orbit.time_dep import time_dep_csns
from orbit.time_dep.waveform import FourierMagnetWaveform, ConstantMagnetWaveform
from lib.read_RF_curve import *
from orbit.diagnostics import TeapotCounterNode

######################################
#define the bend node
#####################################
angMB = pi/12
MB = teapot.BendTEAPOT("bend")
MB.setLength(2.10)
MB.addParam("theta",angMB)
MB.addParam("ea1",angMB/2)
MB.addParam("ea2",angMB/2)
MB.addParam("ratio",(0.5,0.5))

R1MB01 = copy.deepcopy(MB)
R1MB02 = copy.deepcopy(MB)
R1MB03 = copy.deepcopy(MB)
R1MB04 = copy.deepcopy(MB)
R1MB05 = copy.deepcopy(MB)
R1MB06 = copy.deepcopy(MB)

R2MB01 = copy.deepcopy(MB)
R2MB02 = copy.deepcopy(MB)
R2MB03 = copy.deepcopy(MB)
R2MB04 = copy.deepcopy(MB)
R2MB05 = copy.deepcopy(MB)
R2MB06 = copy.deepcopy(MB)

R3MB01 = copy.deepcopy(MB)
R3MB02 = copy.deepcopy(MB)
R3MB03 = copy.deepcopy(MB)
R3MB04 = copy.deepcopy(MB)
R3MB05 = copy.deepcopy(MB)
R3MB06 = copy.deepcopy(MB)

R4MB01 = copy.deepcopy(MB)
R4MB02 = copy.deepcopy(MB)
R4MB03 = copy.deepcopy(MB)
R4MB04 = copy.deepcopy(MB)
R4MB05 = copy.deepcopy(MB)
R4MB06 = copy.deepcopy(MB)


angBC=0.06316
#angBC=0.01
BC01 = teapot.BendTEAPOT("BC")
BC01.setLength(0.35)
BC01.addParam("theta",-angBC)
BC01.addParam("ea1",0)
BC01.addParam("ea2",-angBC)
BC01.addParam("ratio",(0,1))

BC02 = teapot.BendTEAPOT("BC")
BC02.setLength(0.35)
BC02.addParam("theta",angBC)
BC02.addParam("ea1",angBC)
BC02.addParam("ea2",0)
BC02.addParam("ratio",(1,0))

BC03 = teapot.BendTEAPOT("BC")
BC03.setLength(0.35)
BC03.addParam("theta",angBC)
BC03.addParam("ea1",0)
BC03.addParam("ea2",angBC)
BC03.addParam("ratio",(0,1))

BC04 = teapot.BendTEAPOT("BC")
BC04.setLength(0.35)
BC04.addParam("theta",-angBC)
BC04.addParam("ea1",-angBC)
BC04.addParam("ea2",0)
BC04.addParam("ratio",(1,0))


L001_a = teapot.DriftTEAPOT("L001_a")
L001_b = teapot.DriftTEAPOT("L001_b")
L001_c = teapot.DriftTEAPOT("L001_c")
L002 = teapot.DriftTEAPOT("L002")
L003 = teapot.DriftTEAPOT("L003")
L004 = teapot.DriftTEAPOT("L004")
L005 = teapot.DriftTEAPOT("L005")
L006 = teapot.DriftTEAPOT("L006")
L007 = teapot.DriftTEAPOT("L007")
L008 = teapot.DriftTEAPOT("L008")
L009 = teapot.DriftTEAPOT("L009")
L010 = teapot.DriftTEAPOT("L010")
L011 = teapot.DriftTEAPOT("L011")
L012 = teapot.DriftTEAPOT("L012")
L013 = teapot.DriftTEAPOT("L013")
L014 = teapot.DriftTEAPOT("L014")
L015 = teapot.DriftTEAPOT("L015")
L016 = teapot.DriftTEAPOT("L016")
L017 = teapot.DriftTEAPOT("L017")
L018 = teapot.DriftTEAPOT("L018")
L019 = teapot.DriftTEAPOT("L019")
L020 = teapot.DriftTEAPOT("L020")
L021 = teapot.DriftTEAPOT("L021")
L022 = teapot.DriftTEAPOT("L022")
L023 = teapot.DriftTEAPOT("L023")
L024 = teapot.DriftTEAPOT("L024")
L025 = teapot.DriftTEAPOT("L025")
L026 = teapot.DriftTEAPOT("L026")
L027 = teapot.DriftTEAPOT("L027")
L028 = teapot.DriftTEAPOT("L028")
L029 = teapot.DriftTEAPOT("L029")
L030 = teapot.DriftTEAPOT("L030")
L031 = teapot.DriftTEAPOT("L031")
L032 = teapot.DriftTEAPOT("L032")
L033 = teapot.DriftTEAPOT("L033")
L034 = teapot.DriftTEAPOT("L034")
L035 = teapot.DriftTEAPOT("L035")
L036 = teapot.DriftTEAPOT("L036")
L037 = teapot.DriftTEAPOT("L037")
L038 = teapot.DriftTEAPOT("L038")
L039 = teapot.DriftTEAPOT("L039")
L040 = teapot.DriftTEAPOT("L040")
L041 = teapot.DriftTEAPOT("L041")
L042 = teapot.DriftTEAPOT("L042")
L043 = teapot.DriftTEAPOT("L043")
L044 = teapot.DriftTEAPOT("L044")
L045 = teapot.DriftTEAPOT("L045")
L046 = teapot.DriftTEAPOT("L046")
L047 = teapot.DriftTEAPOT("L047")
L048 = teapot.DriftTEAPOT("L048")
L049 = teapot.DriftTEAPOT("L049")
L050 = teapot.DriftTEAPOT("L050")
L051 = teapot.DriftTEAPOT("L051")
L052 = teapot.DriftTEAPOT("L052")
L053 = teapot.DriftTEAPOT("L053")
L054 = teapot.DriftTEAPOT("L054")
L055 = teapot.DriftTEAPOT("L055")
L056 = teapot.DriftTEAPOT("L056")
L057 = teapot.DriftTEAPOT("L057")
L058 = teapot.DriftTEAPOT("L058")
L059 = teapot.DriftTEAPOT("L059")
L060 = teapot.DriftTEAPOT("L060")
L061 = teapot.DriftTEAPOT("L061")
L062 = teapot.DriftTEAPOT("L062")
L063 = teapot.DriftTEAPOT("L063")
L064 = teapot.DriftTEAPOT("L064")
L065 = teapot.DriftTEAPOT("L065")
L066 = teapot.DriftTEAPOT("L066")
L067 = teapot.DriftTEAPOT("L067")
L068 = teapot.DriftTEAPOT("L068")
L069 = teapot.DriftTEAPOT("L069")
L070 = teapot.DriftTEAPOT("L070")
L071 = teapot.DriftTEAPOT("L071")
L072 = teapot.DriftTEAPOT("L072")
L073 = teapot.DriftTEAPOT("L073")
L074 = teapot.DriftTEAPOT("L074")
L075 = teapot.DriftTEAPOT("L075")
L076 = teapot.DriftTEAPOT("L076")
L077 = teapot.DriftTEAPOT("L077")
L078 = teapot.DriftTEAPOT("L078")
L079 = teapot.DriftTEAPOT("L079")
L080 = teapot.DriftTEAPOT("L080")
L081 = teapot.DriftTEAPOT("L081")
L082 = teapot.DriftTEAPOT("L082")
L083 = teapot.DriftTEAPOT("L083")
L084 = teapot.DriftTEAPOT("L084")
L085 = teapot.DriftTEAPOT("L085")
L086 = teapot.DriftTEAPOT("L086")
L087 = teapot.DriftTEAPOT("L087")
L088 = teapot.DriftTEAPOT("L088")
L089 = teapot.DriftTEAPOT("L089")
L090 = teapot.DriftTEAPOT("L090")
L091 = teapot.DriftTEAPOT("L091")
L092 = teapot.DriftTEAPOT("L092")
L093 = teapot.DriftTEAPOT("L093")
L094 = teapot.DriftTEAPOT("L094")
L095 = teapot.DriftTEAPOT("L095")
L096 = teapot.DriftTEAPOT("L096")
L097 = teapot.DriftTEAPOT("L097")
L098 = teapot.DriftTEAPOT("L098")
L099 = teapot.DriftTEAPOT("L099")
L100 = teapot.DriftTEAPOT("L100")
L101 = teapot.DriftTEAPOT("L101")
L102 = teapot.DriftTEAPOT("L102")
L103 = teapot.DriftTEAPOT("L103")
L104 = teapot.DriftTEAPOT("L104")
L105 = teapot.DriftTEAPOT("L105")
L106 = teapot.DriftTEAPOT("L106")
L107 = teapot.DriftTEAPOT("L107")
L108 = teapot.DriftTEAPOT("L108")
L109 = teapot.DriftTEAPOT("L109")
L110 = teapot.DriftTEAPOT("L110")
L111 = teapot.DriftTEAPOT("L111")
L112 = teapot.DriftTEAPOT("L112")
L113 = teapot.DriftTEAPOT("L113")
L114 = teapot.DriftTEAPOT("L114")
L115 = teapot.DriftTEAPOT("L115")
L116 = teapot.DriftTEAPOT("L116")
L117 = teapot.DriftTEAPOT("L117")
L118 = teapot.DriftTEAPOT("L118")
L119 = teapot.DriftTEAPOT("L119")
L120 = teapot.DriftTEAPOT("L120")
L121 = teapot.DriftTEAPOT("L121")
L122 = teapot.DriftTEAPOT("L122")
L123 = teapot.DriftTEAPOT("L123")
L124 = teapot.DriftTEAPOT("L124")
L125 = teapot.DriftTEAPOT("L125")
L126 = teapot.DriftTEAPOT("L126")
L127 = teapot.DriftTEAPOT("L127")
L128 = teapot.DriftTEAPOT("L128")
L129 = teapot.DriftTEAPOT("L129")
L130 = teapot.DriftTEAPOT("L130")
L131 = teapot.DriftTEAPOT("L131")
L132 = teapot.DriftTEAPOT("L132")
L133 = teapot.DriftTEAPOT("L133")
L134 = teapot.DriftTEAPOT("L134")
L135 = teapot.DriftTEAPOT("L135")
L136 = teapot.DriftTEAPOT("L136")
L137 = teapot.DriftTEAPOT("L137")
L138 = teapot.DriftTEAPOT("L138")
L139 = teapot.DriftTEAPOT("L139")
L140 = teapot.DriftTEAPOT("L140")
L141 = teapot.DriftTEAPOT("L141")
L142 = teapot.DriftTEAPOT("L142")
L143 = teapot.DriftTEAPOT("L143")
L144 = teapot.DriftTEAPOT("L144")
L145 = teapot.DriftTEAPOT("L145")
L146 = teapot.DriftTEAPOT("L146")
L147 = teapot.DriftTEAPOT("L147")
L148 = teapot.DriftTEAPOT("L148")
L149 = teapot.DriftTEAPOT("L149")
L150 = teapot.DriftTEAPOT("L150")
L151 = teapot.DriftTEAPOT("L151")
L152 = teapot.DriftTEAPOT("L152")
L153 = teapot.DriftTEAPOT("L153")
L154 = teapot.DriftTEAPOT("L154")
L155_a = teapot.DriftTEAPOT("L155_a")
L155_b = teapot.DriftTEAPOT("L155_b")
L155_c = teapot.DriftTEAPOT("L155_c")

L001_a.setLength(0.3000)
L001_b.setLength(0.6000)
L001_c.setLength(1.9700)
L002.setLength(0.3930)
L003.setLength(1.5370)
L004.setLength(0.8000)
L005.setLength(0.7470)
L006.setLength(0.0000)
L007.setLength(0.4030)
L008.setLength(3.8000)
L009.setLength(1.2000)
L010.setLength(0.5730)
L011.setLength(0.0000)
L012.setLength(0.2770)
L013.setLength(0.2500)
L014.setLength(0.1753)
L015.setLength(0.2517)
L016.setLength(0.0000)
L017.setLength(0.6730)
L018.setLength(0.8000)
L019.setLength(0.9000)
L020.setLength(3.5000)
L021.setLength(0.9000)
L022.setLength(0.8000)
L023.setLength(0.5870)
L024.setLength(0.0000)
L025.setLength(0.3382)
L026.setLength(0.1748)
L027.setLength(0.2500)
L028.setLength(0.2770)
L029.setLength(0.0000)
L030.setLength(0.5730)
L031.setLength(1.2000)
L032.setLength(3.8000)
L033.setLength(0.7520)
L034.setLength(0.0000)
L035.setLength(0.3980)
L036.setLength(0.8000)
L037.setLength(0.3170)
L038.setLength(0.0000)
L039.setLength(10.280)
L040.setLength(0.0000)
L041.setLength(0.4030)
L042.setLength(0.8000)
L043.setLength(0.7475)
L044.setLength(0.0000)
L045.setLength(0.4025)
L046.setLength(3.8000)
L047.setLength(1.2000)
L048.setLength(0.5730)
L049.setLength(0.0000)
L050.setLength(0.2770)
L051.setLength(0.2500)
L052.setLength(0.1748)
L053.setLength(0.2522)
L054.setLength(0.0000)
L055.setLength(0.6730)
L056.setLength(0.8000)
L057.setLength(0.9000)
L058.setLength(3.5000)
L059.setLength(0.9000)
L060.setLength(0.8000)
L061.setLength(0.5870)
L062.setLength(0.0000)
L063.setLength(0.3377)
L064.setLength(0.1753)
L065.setLength(0.2500)
L066.setLength(0.2740)
L067.setLength(0.0000)
L068.setLength(0.5760)
L069.setLength(1.2000)
L070.setLength(2.2295)
L071.setLength(0.0000)
L072.setLength(1.5705)
L073.setLength(1.1500)
L074.setLength(0.8000)
L075.setLength(3.9700)
L076.setLength(0.0000)
L077.setLength(0.3930)
L078.setLength(6.2770)
L079.setLength(0.3600)
L080.setLength(0.4650)
L081.setLength(0.3350)
L082.setLength(0.7470)
L083.setLength(0.0000)
L084.setLength(0.4030)
L085.setLength(1.6825)
L086.setLength(2.1175)
L087.setLength(1.2000)
L088.setLength(0.5730)
L089.setLength(0.0000)
L090.setLength(0.2770)
L091.setLength(0.2500)
L092.setLength(0.1753)
L093.setLength(0.2517)
L094.setLength(0.0000)
L095.setLength(0.6730)
L096.setLength(0.8000)
L097.setLength(0.9000)
L098.setLength(3.5000)
L099.setLength(0.9000)
L100.setLength(0.8000)
L101.setLength(0.5870)
L102.setLength(0.0000)
L103.setLength(0.3382)
L104.setLength(0.1748)
L105.setLength(0.2500)
L106.setLength(0.2770)
L107.setLength(0.0000)
L108.setLength(0.5730)
L109.setLength(1.2000)
L110.setLength(3.8000)
L111.setLength(0.7520)
L112.setLength(0.0000)
L113.setLength(0.3980)
L114.setLength(0.8000)
L115.setLength(0.3170)
L116.setLength(0.0000)
L117.setLength(10.280)
L118.setLength(0.0000)
L119.setLength(0.4030)
L120.setLength(0.8000)
L121.setLength(0.7470)
L122.setLength(0.0000)
L123.setLength(0.4030)
L124.setLength(3.8000)
L125.setLength(1.2000)
L126.setLength(0.5730)
L127.setLength(0.0000)
L128.setLength(0.2770)
L129.setLength(0.2500)
L130.setLength(0.1748)
L131.setLength(0.2522)
L132.setLength(0.0000)
L133.setLength(0.6730)
L134.setLength(0.8000)
L135.setLength(0.9000)
L136.setLength(3.5000)
L137.setLength(0.9000)
L138.setLength(0.8000)
L139.setLength(0.5870)
L140.setLength(0.0000)
L141.setLength(0.3377)
L142.setLength(0.1753)
L143.setLength(0.2500)
L144.setLength(0.2770)
L145.setLength(0.0000)
L146.setLength(0.5730)
L147.setLength(1.2000)
L148.setLength(3.8000)
L149.setLength(0.7520)
L150.setLength(0.0000)
L151.setLength(0.3980)
L152.setLength(0.8000)
L153.setLength(1.8670)
L154.setLength(0.0000)
L155_a.setLength(2.0330)
L155_b.setLength(0.6000)
L155_c.setLength(0.3000)

##########################################
# Define quadrupoles
#########################################
KQF01 = 0.977431
KQD02 = -0.764576
KQF03 = 0.876528
KQF04 = 0.752955
KQF06 = 0.802971

#Brhoinj_60 = 1.1486
Brhoinj = 1.319681623

#The main quadrupoles in the ring
QF01 = teapot.QuadTEAPOT("QF01")
QF01.setLength(0.41)
QF01.addParam("kq",KQF01/Brhoinj)
#QF01.addParam("poles",[2,3])
#QF01.addParam("kls",[0.8e-3,0.8e-3])
#QF01.addParam("skews",[0,0])

QD02 = teapot.QuadTEAPOT("QD02")
QD02.setLength(0.9)
QD02.addParam("kq",KQD02/Brhoinj)
#QD02.addParam("poles",[2,3])
#QD02.addParam("kls",[0.8e-3,0.8e-3])
#QD02.addParam("skews",[0,0])

QF03 = teapot.QuadTEAPOT("QF03")
QF03.setLength(0.41)
QF03.addParam("kq",KQF03/Brhoinj)
#QF03.addParam("poles",[2,3])
#QF03.addParam("kls",[0.8e-3,0.8e-3])
#QF03.addParam("skews",[0,0])

QF04 = teapot.QuadTEAPOT("QF04")
QF04.setLength(0.45)
QF04.addParam("kq",KQF04/Brhoinj)
#QF04.addParam("poles",[2,3])
#QF04.addParam("kls",[0.8e-3,0.8e-3])
#QF04.addParam("skews",[0,0])

QF06 = teapot.QuadTEAPOT("QF06")
QF06.setLength(0.62)
QF06.addParam("kq",KQF06/Brhoinj)
#QF06.addParam("poles",[2,3])
#QF06.addParam("kls",[0.8e-3,0.8e-3])
#QF06.addParam("skews",[0,0])

R1QF01 = copy.deepcopy(QF01)
R1QD02 = copy.deepcopy(QD02)
R1QF03 = copy.deepcopy(QF03)
R1QF04 = copy.deepcopy(QF04)
R1QD05 = copy.deepcopy(QD02)
R1QF06 = copy.deepcopy(QF06)
R1QF07 = copy.deepcopy(QF06)
R1QD08 = copy.deepcopy(QD02)
R1QF09 = copy.deepcopy(QF04)
R1QF10 = copy.deepcopy(QF03)
R1QD11 = copy.deepcopy(QD02)
R1QF12 = copy.deepcopy(QF01)

R2QF01 = copy.deepcopy(QF01)
R2QD02 = copy.deepcopy(QD02)
R2QF03 = copy.deepcopy(QF03)
R2QF04 = copy.deepcopy(QF04)
R2QD05 = copy.deepcopy(QD02)
R2QF06 = copy.deepcopy(QF06)
R2QF07 = copy.deepcopy(QF06)
R2QD08 = copy.deepcopy(QD02)
R2QF09 = copy.deepcopy(QF04)
R2QF10 = copy.deepcopy(QF03)
R2QD11 = copy.deepcopy(QD02)
R2QF12 = copy.deepcopy(QF01)

R3QF01 = copy.deepcopy(QF01)
R3QD02 = copy.deepcopy(QD02)
R3QF03 = copy.deepcopy(QF03)
R3QF04 = copy.deepcopy(QF04)
R3QD05 = copy.deepcopy(QD02)
R3QF06 = copy.deepcopy(QF06)
R3QF07 = copy.deepcopy(QF06)
R3QD08 = copy.deepcopy(QD02)
R3QF09 = copy.deepcopy(QF04)
R3QF10 = copy.deepcopy(QF03)
R3QD11 = copy.deepcopy(QD02)
R3QF12 = copy.deepcopy(QF01)

R4QF01 = copy.deepcopy(QF01)
R4QD02 = copy.deepcopy(QD02)
R4QF03 = copy.deepcopy(QF03)
R4QF04 = copy.deepcopy(QF04)
R4QD05 = copy.deepcopy(QD02)
R4QF06 = copy.deepcopy(QF06)
R4QF07 = copy.deepcopy(QF06)
R4QD08 = copy.deepcopy(QD02)
R4QF09 = copy.deepcopy(QF04)
R4QF10 = copy.deepcopy(QF03)
R4QD11 = copy.deepcopy(QD02)
R4QF12 = copy.deepcopy(QF01)

###################################
#The sextupoles 
###################################
KSF01 = 0;
KSD02 = 0;
SF = teapot.MultipoleTEAPOT("sextupole_SF")
SF.setLength(0.2)
SF.getParam("poles").append(2)
SF.getParam("kls").append(KSF01)
SF.getParam("skews").append(0)

SD = teapot.MultipoleTEAPOT("sextupole_SD")
SD.setLength(0.2)
SD.getParam("poles").append(2)
SD.getParam("kls").append(KSD02)
SD.getParam("skews").append(0)

R1SF01 = copy.deepcopy(SF)
R1SD02 = copy.deepcopy(SD)
R1SD03 = copy.deepcopy(SD)
R1SF04 = copy.deepcopy(SF)

R2SF01 = copy.deepcopy(SF)
R2SD02 = copy.deepcopy(SD)
R2SD03 = copy.deepcopy(SD)
R2SF04 = copy.deepcopy(SF)

R3SF01 = copy.deepcopy(SF)
R3SD02 = copy.deepcopy(SD)
R3SD03 = copy.deepcopy(SD)
R3SF04 = copy.deepcopy(SF)

R4SF01 = copy.deepcopy(SF)
R4SD02 = copy.deepcopy(SD)
R4SD03 = copy.deepcopy(SD)
R4SF04 = copy.deepcopy(SF)


#BPM list

R3LMTDH = teapot.MonitorTEAPOT("LMT")
R3LMTDV = teapot.MonitorTEAPOT("LMT")

R1BPM01 = teapot.MonitorTEAPOT("R1BPM01")
R1BPM02 = teapot.MonitorTEAPOT("R1BPM02")
R1BPM04 = teapot.MonitorTEAPOT("R1BPM04")
R1BPM05 = teapot.MonitorTEAPOT("R1BPM05")
R1BPM08 = teapot.MonitorTEAPOT("R1BPM08")
R1BPM09 = teapot.MonitorTEAPOT("R1BPM09")
R1BPM11 = teapot.MonitorTEAPOT("R1BPM11")
R1BPM12 = teapot.MonitorTEAPOT("R1BPM12")

R2BPM01 = teapot.MonitorTEAPOT("R2BPM01")
R2BPM02 = teapot.MonitorTEAPOT("R2BPM02")
R2BPM04 = teapot.MonitorTEAPOT("R2BPM04")
R2BPM05 = teapot.MonitorTEAPOT("R2BPM05")
R2BPM08 = teapot.MonitorTEAPOT("R2BPM08")
R2BPM09 = teapot.MonitorTEAPOT("R2BPM09")
R2BPM11 = teapot.MonitorTEAPOT("R2BPM11")
R2BPM12 = teapot.MonitorTEAPOT("R2BPM12")

R3BPM01 = teapot.MonitorTEAPOT("R3BPM01")
R3BPM02 = teapot.MonitorTEAPOT("R3BPM02")
R3BPM04 = teapot.MonitorTEAPOT("R3BPM04")
R3BPM05 = teapot.MonitorTEAPOT("R3BPM05")
R3BPM08 = teapot.MonitorTEAPOT("R3BPM08")
R3BPM09 = teapot.MonitorTEAPOT("R3BPM09")
R3BPM11 = teapot.MonitorTEAPOT("R3BPM11")
R3BPM12 = teapot.MonitorTEAPOT("R3BPM12")

R4BPM01 = teapot.MonitorTEAPOT("R4BPM01")
R4BPM02 = teapot.MonitorTEAPOT("R4BPM02")
R4BPM04 = teapot.MonitorTEAPOT("R4BPM04")
R4BPM05 = teapot.MonitorTEAPOT("R4BPM05")
R4BPM08 = teapot.MonitorTEAPOT("R4BPM08")
R4BPM09 = teapot.MonitorTEAPOT("R4BPM09")
R4BPM11 = teapot.MonitorTEAPOT("R4BPM11")
R4BPM12 = teapot.MonitorTEAPOT("R4BPM12")


#The correctors

R1DH01 = teapot.KickTEAPOT("R1DH01")
R1DV02 = teapot.KickTEAPOT("R1DV02")
R1DH04 = teapot.KickTEAPOT("R1DH04")
R1DV05 = teapot.KickTEAPOT("R1DV05")
R1DV08 = teapot.KickTEAPOT("R1DV08")
R1DH09 = teapot.KickTEAPOT("R1DH09")
R1DV11 = teapot.KickTEAPOT("R1DV11")
R1DH12 = teapot.KickTEAPOT("R1DH12")

R2DH01 = teapot.KickTEAPOT("R2DH01")
R2DV02 = teapot.KickTEAPOT("R2DV02")
R2DH04 = teapot.KickTEAPOT("R2DH04")
R2DV05 = teapot.KickTEAPOT("R2DV05")
R2DV08 = teapot.KickTEAPOT("R2DV08")
R2DH09 = teapot.KickTEAPOT("R2DH09")
R2DV11 = teapot.KickTEAPOT("R2DV11")
R2DH12 = teapot.KickTEAPOT("R2DH12")

R3DH01 = teapot.KickTEAPOT("R3DH01")
R3DV02 = teapot.KickTEAPOT("R3DV02")
R3DH04 = teapot.KickTEAPOT("R3DH04")
R3DV05 = teapot.KickTEAPOT("R3DV05")
R3DV08 = teapot.KickTEAPOT("R3DV08")
R3DH09 = teapot.KickTEAPOT("R3DH09")
R3DV11 = teapot.KickTEAPOT("R3DV11")
R3DH12 = teapot.KickTEAPOT("R3DH12")

R4DH01 = teapot.KickTEAPOT("R4DH01")
R4DV02 = teapot.KickTEAPOT("R4DV02")
R4DH04 = teapot.KickTEAPOT("R4DH04")
R4DV05 = teapot.KickTEAPOT("R4DV05")
R4DV08 = teapot.KickTEAPOT("R4DV08")
R4DH09 = teapot.KickTEAPOT("R4DH09")
R4DV11 = teapot.KickTEAPOT("R4DV11")
R4DH12 = teapot.KickTEAPOT("R4DH12")

counter = TeapotCounterNode("counter") # record the tracking turn, only need one

RCS=[counter,L001_a,BC01,L001_b,BC02,L001_c,R1BPM01,L002,R1DH01,L003,R1QF01,L004,R1QD02,L005,R1BPM02,L006,R1DV02,\
L007,R1QF03,L008,R1MB01,L009,R1MB02,L010,R1BPM04,L011,R1DH04,L012,R1SF01,L013,\
R1QF04,L014,R1SD02,L015,R1BPM05,L016,R1DV05,L017,R1QD05,L018,R1QF06,L019,R1MB03,L020,R1MB04,L021,R1QF07,L022,R1QD08,L023,R1BPM08,L024,R1DV08,\
L025,R1SD03,L026,R1QF09,L027,R1SF04,L028,R1BPM09,L029,R1DH09,L030,R1MB05,L031,R1MB06,L032,R1QF10,L033,R1BPM11,L034,R1DV11,L035,\
R1QD11,L036,R1QF12,L037,R1BPM12,L038,R1DH12,L039,R2BPM12,L040,R2DH12,L041,R2QF12,L042,R2QD11,L043,R2BPM11,L044,R2DV11,L045,\
R2QF10,L046,R2MB06,L047,R2MB05,L048,R2BPM09,L049,R2DH09,L050,R2SF04,L051,R2QF09,L052,R2SD03,L053,R2BPM08,L054,R2DV08,L055,R2QD08,\
L056,R2QF07,L057,R2MB04,L058,R2MB03,L059,R2QF06,L060,R2QD05,L061,R2BPM05,L062,R2DV05,L063,R2SD02,L064,R2QF04,L065,R2SF01,L066,\
R2BPM04,L067,R2DH04,L068,R2MB02,L069,R2MB01,L070,R2BPM02,L071,R2DV02,L072,R2QF03,L073,R2QD02,L074,R2QF01,L075,R2BPM01,L076,R2DH01,L077,R3LMTDV,L078,R3BPM01,L079,\
R3QF01,L080,R3DH01,L081,R3QD02,L082,R3BPM02,L083,R3DV02,L084,R3QF03,L085,R3LMTDH,L086,R3MB01,L087,R3MB02,L088,R3BPM04,L089,R3DH04,L090,R3SF01,L091,R3QF04,\
L092,R3SD02,L093,R3BPM05,L094,R3DV05,L095,R3QD05,L096,R3QF06,L097,R3MB03,L098,R3MB04,L099,R3QF07,L100,R3QD08,L101,R3BPM08,L102,R3DV08,L103,R3SD03,L104,\
R3QF09,L105,R3SF04,L106,R3BPM09,L107,R3DH09,L108,R3MB05,L109,R3MB06,L110,R3QF10,L111,R3BPM11,L112,R3DV11,L113,R3QD11,L114,R3QF12,L115,R3BPM12,L116,R3DH12,L117,\
R4BPM12,L118,R4DH12,L119,R4QF12,L120,R4QD11,L121,R4BPM11,L122,R4DV11,L123,R4QF10,L124,R4MB06,L125,R4MB05,L126,R4BPM09,L127,R4DH09,L128,R4SF04,L129,R4QF09,\
L130,R4SD03,L131,R4BPM08,L132,R4DV08,L133,R4QD08,L134,R4QF07,L135,R4MB04,L136,R4MB03,L137,R4QF06,L138,R4QD05,L139,R4BPM05,L140,R4DV05,L141,R4SD02,L142,\
R4QF04,L143,R4SF01,L144,R4BPM04,L145,R4DH04,L146,R4MB02,L147,R4MB01,L148,R4QF03,L149,R4BPM02,L150,R4DV02,L151,R4QD02,L152,R4QF01,L153,R4BPM01,L154,R4DH01,\
L155_a,BC03,L155_b,BC04,L155_c];

lattice = time_dep_csns.TIME_DEP_Lattice()
lattice.setNodes(RCS)
#lattice.addChildren()
lattice.initialize()

##########################################################
#Main bunch parameters
##########################################################
b = Bunch()
syncPart = b.getSyncParticle()
energy_inj = 0.08 #GeV
energy_ext = 1.6 # GeV
syncPart.kinEnergy(energy_inj)
paramDict={}
lostbunch = Bunch()
lostfoilbunch = Bunch()
paramDict["lostbunch"] = lostbunch
paramDict["bunch"] = b
paramDict["lostfoilbunch"] = lostfoilbunch
lostbunch.addPartAttr("LostParticleAttributes")

###############################################
#Define the RF cavity
###############################################
[time,values,RFV,RFphase,harms] = read_RF_curve('./input/rfdata1.dat', True)
ZtoPhi = 2.0*pi/lattice.getLength()
accelDict = {}
accelDict["gammaTrans"] = 4.722
accelDict["RFHNum"] = harms
accelDict["RFVoltage"] = RFV
accelDict["RFPhase"] = RFphase
accelDict["n_tuple"] = len(time)-1
accelDict["time"] = time
accelDict["SyncPhase"] = values
accelDict["BRho"] = values
length = 0.0
name = "RF_node"
#R1CAV01 = RFNode.SyncPhaseDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
R1CAV01 = RFNode.BRhoDep_Multi_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)

#R1CAV01 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R1CAV02 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R3CAV01 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R3CAV02 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R3CAV03 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R4CAV01 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R4CAV02 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)
#R4CAV03 = RFNode.BRhoDep_Harmonic_RFNode(ZtoPhi, accelDict,b,length,name)

RFLatticeModifications.addRFNode(lattice,10.79735,R1CAV01)
#RFLatticeModifications.addRFNode(lattice,46.18435,R1CAV02)
#RFLatticeModifications.addRFNode(lattice,160.14435,R3CAV01)
#RFLatticeModifications.addRFNode(lattice,167.41735,R3CAV02)
#RFLatticeModifications.addRFNode(lattice,170.127347,R3CAV03)
#RFLatticeModifications.addRFNode(lattice,172.837344,R4CAV01)
#RFLatticeModifications.addRFNode(lattice,181.73735,R4CAV02)
#RFLatticeModifications.addRFNode(lattice,217.12435,R4CAV03)


#####################################
#Define the collimator
####################################
ma = 9# material type, 1=carbon, 2=aluminum, 3=iron, 4=copper, 5=tantalum, 6=tungstun, 7=platinum, 8=lead, 9 = black absorber
density_fac = 1.0 #A multiplier on the density of chosen material, default =1
shape = 1 # shape of the collimator 1=circle, 2=ellipse, 3=one sided flat, 4=two sided flat, 5=rectangular (outside is collimator), 6=rectangular (inside is collimator)
radiusP = 0.14
radiusS = 0.114

collimatorP = TeapotCollimatorNode(0.0002, ma, density_fac, shape, radiusP, 0., 0., 0., 0.,0.0, "Col_prime")
collimatorS1 = TeapotCollimatorNode(0.2, ma, density_fac, shape, radiusS, 0., 0., 0., 0.,0.0, "Col_sec1")
collimatorS2 = TeapotCollimatorNode(0.2, ma, density_fac, shape, radiusS, 0., 0., 0., 0.,0.0, "Col_sec2")
collimatorS3 = TeapotCollimatorNode(0.2, ma, density_fac, shape, radiusS, 0., 0., 0., 0.,0.0, "Col_sec3")
collimatorS4 = TeapotCollimatorNode(0.2, ma, density_fac, shape, radiusS, 0., 0., 0., 0.,0.0, "Col_sec4")
addTeapotCollimatorNode(lattice, 52.715, collimatorP)
addTeapotCollimatorNode(lattice, 53.713, collimatorS1)
addTeapotCollimatorNode(lattice, 55.313, collimatorS2)
addTeapotCollimatorNode(lattice, 57.313, collimatorS3)
addTeapotCollimatorNode(lattice, 60.113, collimatorS4)

######################################################
# add kickers as painting magnet
# kicker node should be added before spacecharge node
######################################################
t1 = 0
t2 = 0.00039 #s
startampx = 0.033#m
endampx = 0.00
startampy = -0.033
endampy = 0.00

paintingWavex = RangeRootTWaveform(syncPart, t1, t2, startampx, endampx)
paintingWavey = RangeRootTWaveform(syncPart, t1, t2, startampy, endampy)
strengthX = 1/2.2
strengthY = 1/2.2

BH01 = TeapotXKickerNode(b, -strengthX, paintingWavex, 'BH')
BH02 = TeapotXKickerNode(b, strengthX, paintingWavex, 'BH')
BH03 = TeapotXKickerNode(b, strengthX, paintingWavex, 'BH')
BH04 = TeapotXKickerNode(b, -strengthX, paintingWavex, 'BH')
BV01 = TeapotYKickerNode(b, -strengthY, paintingWavey, 'BV')
BV02 = TeapotYKickerNode(b, strengthY, paintingWavey, 'BV')
BV03 = TeapotYKickerNode(b, strengthY, paintingWavey, 'BV')
BV04 = TeapotYKickerNode(b, -strengthY, paintingWavey, 'BV')

addTeapotKickerNode(lattice, 2.155, BH01)
addTeapotKickerNode(lattice, 4.355, BH02)
addTeapotKickerNode(lattice, 223.565, BH03)
addTeapotKickerNode(lattice, 225.765, BH04)
addTeapotKickerNode(lattice, 2.705, BV01)
addTeapotKickerNode(lattice, 4.905, BV02)
addTeapotKickerNode(lattice, 223.015, BV03)
addTeapotKickerNode(lattice, 225.215, BV04)


########################################
#add apertures after add painting kickers
########################################
#add_CSNS_RCS_Aperture(lattice)

#############################################################
#Add spacecharge nodes to the lattice
#############################################################
sizeX = 128 #number of grid points in horizontal direction
sizeY = 128 #number of grid points in vertical direction
sizeZ = 128   #number of logitudinal slices in the 2.5D space charge solver

n_boundary_points = 100
n_freespace_modes = 20
boundary_shape = "Circle"
a_semi = 0.01
b_semi = 0.01
boundary = Boundary2D(n_boundary_points, n_freespace_modes, boundary_shape,
        a_semi, b_semi)
calc2p5d = SpaceChargeCalc2p5D(sizeX, sizeY, sizeZ)
#m if the distance of two nodes less than this value, we will not add sc node in the latter one
sc_path_length_min = 0.001
#setSC2p5DAccNodes(lattice, sc_path_length_min, calc2p5d, boundary)

##################################################
#prepare to define time dependent lattice
#################################################

lattice.setLatticeOrder()

## The magnet strength variation subject to a Fourier series
## f(t) = a+b*sin(w*t+p)+b1*sin(2*w*t+p1)+b2*sin(3*w*t+p2)......
## where a is the DC component; b,b1,b2... are the amplitudes of each order;
## w is the frequency of the machine, for CSNS/RCS which is 25 Hz;
## p,p1,p2... are the phase of each order.

#f01_min = 0.977431
#f01_max = 5.8268
QF01_a = 3.4021155
QF01_w = 25
#8 quadrupoles of QF01
QF01_b = [[2.4246845,],
          [2.4246845,],
          [2.4246845,],
          [2.4246845,],
          [2.4246845,],
          [2.4246845,],
          [2.4246845,],
          [2.4246845,]]
## There are 8 arrays for 8 QF01, actually the 8 QF01 are powered by the same powersupply,
## in princle, we can use just one array for all the 8 quads, but here we prepare 8 arrays,
## it's useful when we need to simulate the effects of the errors during these 8 quads.
QF01_p = [[-0.5,],
          [-0.5,],
          [-0.5,],
          [-0.5,],
          [-0.5,],
          [-0.5,],
          [-0.5,],
          [-0.5,]]

#f02_min = -0.764576
#f02_max = -4.5578
QD02_a = -2.661188
QD02_w = 25
#16 quadrupoles of QD
QD02_b = [[-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,],
          [-1.896612,]]

QD02_p = [[-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5]]

# 8 quadrupoles of QF03
#f03_min = 0.876528
#f03_max = 5.225283
QF03_a = 3.0509055
QF03_w = 25
QF03_b = [[2.1743775,],
          [2.1743775,],
          [2.1743775,],
          [2.1743775,],
          [2.1743775,],
          [2.1743775,],
          [2.1743775,],
          [2.1743775,]]
QF03_p = [[-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5]]

#8 quadrupoles of QF04
#f04_min = 0.752955
#f04_max = 4.488622
QF04_a = 2.6207885
QF04_w = 25
QF04_b = [[1.8678335,],
          [1.8678335,],
          [1.8678335,],
          [1.8678335,],
          [1.8678335,],
          [1.8678335,],
          [1.8678335,],
          [1.8678335,]]

QF04_p = [[-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5]]

# 8 quadrupoles of QF06
#f06_min = 0.802971
#f06_max = 4.786784
QF06_a = 2.7948775
QF06_w = 25
QF06_b = [[1.9919065,],
          [1.9919065,],
          [1.9919065,],
          [1.9919065,],
          [1.9919065,],
          [1.9919065,],
          [1.9919065,],
          [1.9919065,]]
QF06_p = [[-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5],
          [-0.5]]

BCStr=[-0.238146,0.238146,0.238146,-0.238146]

(i,j,k,m,n,z)=(0,0,0,0,0,0)
for node in lattice.getNodes():
        name =  node.getParam('TPName')
        if name.find('QF01') == 0:
                lattice.setTimeDepNode(name,FourierMagnetWaveform(QF01_a,QF01_b[i],QF01_w,QF01_p[i]))
                i+=1
        elif name.find('QD02') == 0:
                lattice.setTimeDepNode(name,FourierMagnetWaveform(QD02_a,QD02_b[j],QD02_w,QD02_p[j]))
                j+=1
        elif name.find('QF03') == 0:
                lattice.setTimeDepNode(name,FourierMagnetWaveform(QF03_a,QF03_b[k],QF03_w,QF03_p[k]))
                k+=1
        elif name.find('QF04') == 0:
                lattice.setTimeDepNode(name,FourierMagnetWaveform(QF04_a,QF04_b[m],QF04_w,QF04_p[m]))
                m+=1
        elif name.find('QF06') == 0:
                lattice.setTimeDepNode(name,FourierMagnetWaveform(QF06_a,QF06_b[n],QF06_w,QF06_p[n]))
                n+=1
        elif name.find('BC') == 0:
                lattice.setTimeDepNode(name,ConstantMagnetWaveform(BCStr[z]))
                z+=1
        else:
                pass

############################################################
#calculate the tune and twiss parameters for the ac lattice
###########################################################
from orbit.time_dep import time_dep_matrix_lattice

proton=0.93827231 #GeV
b_test = Bunch()
syncP = b_test.getSyncParticle()
energy = 0.08 #GeV
syncP.kinEnergy(energy)
matrix = time_dep_matrix_lattice.TIME_DEP_MATRIX_Lattice(lattice, b_test)
ring_par_dict = matrix.getRingParametersDict()
tune_x = ring_par_dict["fractional tune x"]
tune_y = ring_par_dict["fractional tune y"]
beta_x = ring_par_dict["beta x [m]"]
a=ring_par_dict["transition energy [GeV]"]
gamma_trans = ring_par_dict["gamma transition"]
print '# Energy(GeV),Tunex,Tuney,gamma_trans,compaction_factor'
print energy,1+tune_x,1+tune_y, gamma_trans, (1/gamma_trans)**2

#set the time and related energy and rebuild, then you can get the twiss parameters in that moment
b_test.getSyncParticle().time(0.02)
b_test.getSyncParticle().kinEnergy(1.6)
matrix.rebuild(b_test)

ring_par_dict = matrix.getRingParametersDict()
tune_x = ring_par_dict["fractional tune x"]
tune_y = ring_par_dict["fractional tune y"]
beta_x = ring_par_dict["beta x [m]"]
a=ring_par_dict["transition energy [GeV]"]
gamma_trans = ring_par_dict["gamma transition"]
print 1.6,1+tune_x,1+tune_y,gamma_trans,(1/gamma_trans)**2
