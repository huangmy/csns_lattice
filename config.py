import json
import time

cf = {}
#######################################
#some general parameters
config = dict()
config['name'] = 'design'
config['author'] = 'xiaohanlu'
config['date'] = time.time()

######################################
#machine parameters
machine = dict()
machine['phase'] = 1
machine['model'] = 'AC'
machine['sliced'] = False
machine['energy_inj'] = 0.08
machine['energy_ext'] = 1.6

#####################################
#tracking configuration
track = dict()
track['turn'] = 200
track['info'] = True

######################################
# config of the diagnostic nodes
diag = dict()
dump = {}
stat = {}
tune = {}
bpm = {}
diag['dump'] = dump
diag['stat'] = stat
diag['bpm'] = bpm
diag['tune'] = tune
dump['enable'] = True
dump['folder'] = './output/dump/'
dump['name'] = 'dump.bunch'
dump['positions'] = [0.001]
dump['interval'] = 100 #every 100 turn dump the bunch
dump['start'] = 1
dump['end'] = 2000
stat['enable'] = False#write out the beam info every turn at specified postions
stat['folder'] = './output/stat/'
stat['name'] = 'stat.dat'
stat['positions'] = [0.0010001,]
tune['enable'] = False
tune['folder'] = './output/tune/'
tune['name'] = 'tune.dat'
tune['point'] = 20 # do the calculation at this turn
tune['thruput'] = 1000 # how many particles to be calculated
tune['sample_size'] = 10 #how many turns used to do the calculation(before point)
bpm['folder'] = './output/bpm'
bpm["R1BPM01"] = False
bpm["R1BPM02"] = False
bpm["R1BPM04"] = False
bpm["R1BPM05"] = False
bpm["R1BPM08"] = False
bpm["R1BPM09"] = False
bpm["R1BPM11"] = False
bpm["R1BPM12"] = False
bpm["R2BPM01"] = False
bpm["R2BPM02"] = False
bpm["R2BPM04"] = False
bpm["R2BPM05"] = False
bpm["R2BPM08"] = False
bpm["R2BPM09"] = False
bpm["R2BPM11"] = False
bpm["R2BPM12"] = False
bpm["R3BPM01"] = False
bpm["R3BPM02"] = False
bpm["R3BPM04"] = False
bpm["R3BPM05"] = False
bpm["R3BPM08"] = False
bpm["R3BPM09"] = False
bpm["R3BPM11"] = False
bpm["R3BPM12"] = False
bpm["R4BPM01"] = False
bpm["R4BPM02"] = False
bpm["R4BPM04"] = False
bpm["R4BPM05"] = False
bpm["R4BPM08"] = False
bpm["R4BPM09"] = False
bpm["R4BPM11"] = False
bpm["R4BPM12"] = False

######################################
#the config of injection
inj = {}
beam = {}
dist = {}
foil = {}
inj['beam'] = beam
inj['distribution'] = dist
inj['foil'] = foil
beam['intensity'] = 1.56e13
beam['injectionturn'] = 200
beam['macroperturn'] = 400 #how many particles will be injected each turn
foil['xmin'] = -0.05
foil['xmax'] = 0.05
foil['ymin'] = -0.05
foil['ymax'] = 0.05
dist['type'] = 1
dist['order'] = 12
dist['alphax'] = 0.00306
dist['betax'] = 1.6306
dist['alphay'] = -0.10154
dist['betay'] = 1.6369
dist['emitx'] = 0.636
dist['emity'] = 0.616
dist['xcenterpos'] = 0.033
dist['xcentermom'] = 0.0
dist['ycenterpos'] = 0.0
dist['ycentermom'] = 0.0
dist['tailfrac'] = 0
dist['tailfac'] = 1
dist['bunchPhaseLength'] = 180 #degree 
dist['chopper_duty'] = 50
dist['emean'] = 0.08
dist['esigma'] = 0.000015
dist['etrunc'] = 1

######################################
#the value of main magnet
magnet = dict()
magnet['BC'] = 0.06316
magnet['KQF01'] = 0.977431 #gradient of quadropule
magnet['KQD02'] = -0.764576
magnet['KQF03'] = 0.876528
magnet['KQF04'] = 0.752955
magnet['KQF06'] = 0.802971
magnet['KSF01'] = 0
magnet['KSD02'] = 0

#####################################
#the value of correctors
corr = dict()
dh = {}
dv = {}
tms = {}
corr['time']=tms
tms['start_time'] = 0
tms['end_time'] = 0.002
tms['time_tuple'] = list(range(21))
corr['dh'] = dh
corr['dv'] = dv
if machine['model'] == "AC":
    zerotuple = 21*[0] #default
else:
    zerotuple = 0
dh['R1DH01'] = zerotuple
dh['R1DH04'] = zerotuple
dh['R1DH09'] = zerotuple
dh['R1DH12'] = zerotuple
dh['R2DH01'] = zerotuple
dh['R2DH04'] = zerotuple
dh['R2DH09'] = zerotuple
dh['R2DH12'] = zerotuple
dh['R3DH01'] = zerotuple
dh['R3DH04'] = zerotuple
dh['R3DH09'] = zerotuple
dh['R3DH12'] = zerotuple
dh['R4DH01'] = zerotuple
dh['R4DH04'] = zerotuple
dh['R4DH09'] = zerotuple
dh['R4DH12'] = zerotuple

dv['R1DV02'] = zerotuple
dv['R1DV05'] = zerotuple
dv['R1DV08'] = zerotuple
dv['R1DV11'] = zerotuple
dv['R2DV02'] = zerotuple
dv['R2DV05'] = zerotuple
dv['R2DV08'] = zerotuple
dv['R2DV11'] = zerotuple
dv['R3DV02'] = zerotuple
dv['R3DV05'] = zerotuple
dv['R3DV08'] = zerotuple
dv['R3DV11'] = zerotuple
dv['R4DV02'] = zerotuple
dv['R4DV05'] = zerotuple
dv['R4DV08'] = zerotuple
dv['R4DV11'] = zerotuple

#####################################
#the config of rf cavity
rf = dict()
rf['enable'] = True
#for ac
rf['curve'] = './input/rfdata1.multi'
rf['multi'] = True
rf['tracker'] = 1
#for dc
rf['desync'] = 0
rf['num'] = 2
rf['voltage'] = 2.4e-6
rf['phase'] = 0
rf['tracker'] = 1

#####################################
# the config of collimation
coll = dict()
coll['enable'] = False
coll['mtype'] = 9
coll['shape'] = 1
coll['density_fac'] = 1.0
coll['primary'] = 0.14
coll['secondary'] = 0.114

####################################
#switch of aperture
aper = dict()
aper['enable'] = True

####################################
# the config of painting
paint = dict()
paint['enable'] = True
paint['start_time'] = 0
paint['end_time'] = 0.00039
paint['start_ampx'] = 0.033
paint['end_ampx'] = 0.00
paint['start_ampy'] = -0.033
paint['end_ampy'] = 0.00
paint['waveform'] = 'RangeRootT'

##########################################
#the config of spacecharge calculation
sc = dict()
sc['solver'] = 1
sc['sizex'] = 128
sc['sizey'] = 128
sc['sizez'] = 128
sc['boundary_points'] = 100
sc['free_mode'] = 20
sc['shape'] = 1 # 1. Circle 2. Ellipse 3. Rectangle
sc['shape_a'] = 0.01
sc['shape_b'] = 0.01
sc['enable'] = False

############################################
#the config of ac magnet
## The magnet strength variation subject to a Fourier series
## f(t) = a+b*sin(w*t+p)+b1*sin(2*w*t+p1)+b2*sin(3*w*t+p2)......
## where a is the DC component; b,b1,b2... are the amplitudes of each order;
## w is the frequency of the machine, for CSNS/RCS which is 25 Hz;
## w is fixed for CSNS/RCS, so there is no need to prepare a dedicated variable for it
## p,p1,p2... are the phase of each order.
ac = dict()
ac['QF01'] = {}
ac['QD02'] = {}
ac['QF03'] = {}
ac['QF04'] = {}
ac['QF06'] = {}
ac['QF01']['a'] = 3.4021155
ac['QF01']['b'] = [2.4246845,]
ac['QF01']['p'] = [-0.5,]
ac['QD02']['a'] = -2.661188
ac['QD02']['b'] = [-1.896612,]
ac['QD02']['p'] = [-0.5,]
ac['QF03']['a'] = 3.0509055
ac['QF03']['b'] = [2.1743775,]
ac['QF03']['p'] = [-0.5,]
ac['QF04']['a'] = 2.6207885
ac['QF04']['b'] = [1.8678335,]
ac['QF04']['p'] = [-0.5,]
ac['QF06']['a'] = 2.7948775
ac['QF06']['b'] = [1.9919065,]
ac['QF06']['p'] = [-0.5,]
ac['BC'] = 0.238146
#ac['BC'] = 1e-36

###################################################
##ready to write out
cf['config'] = config
cf['machine'] = machine
cf['injection'] = inj
cf['magnet'] = magnet
cf['corrector'] = corr
cf['rfcavity'] = rf
cf['collimator'] = coll
cf['painting'] = paint
cf['spacecharge']=sc
cf['timedependent'] = ac
cf['diagnostic'] = diag
cf['aperture'] = aper
cf['track'] = track

if __name__ == "__main__":
    with open('config.json', 'w') as f_out:
        json.dump(cf, f_out)
