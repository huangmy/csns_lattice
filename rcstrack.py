#!/usr/bin/env pyORBIT
import time
import orbit_mpi
from lib.timecalc import timecalc
from lib import utils
from orbit.time_dep import time_dep_csns
from orbit.injection import TeapotInjectionNode, addTeapotInjectionNode
from orbit.injection import JohoTransverse, GULongDist
from orbit.diagnostics import addTeapotDiagnosticsNode,TeapotDumpNode,TeapotStatLatsNode
#from lattice_csns import lattice,b,paramDict # used for DC mode
from TD_lat_csns import lattice,b,paramDict # used for AC mode


####################################################
#the bunch 'b' import from lattice file 
#to keep consistent with some nodes which need to 
#be defined with a bunch or syncParticle
#################################################
intensity = 1.56e13 # the total number of the particles
injectionturns = 200 # injection turn
macroperturn = 200 # injected particle numbers per turn
macrosize = intensity/injectionturns/macroperturn
lostfoilbunch = paramDict["lostfoilbunch"]

b.macroSize(macrosize)# It's very import for space charge calculation, if not assign, the calculator will crash out
syncPart = b.getSyncParticle()

############################################################
#Add Injection Node to inject particles
#The injection node should be added at last to 
#make sure it's at the beginning of the lattice
###########################################################
xmin = -0.050
xmax = 0.050
ymin = -0.050
ymax = 0.050

foilparams = (xmin, xmax, ymin, ymax)

order = 12.
alphax = 0.00306
betax = 1.6309 #m
alphay = -0.10154
betay = 1.6369
emitxlim = 0.636 * 2*(order + 1) * 1e-6
emitylim = 0.616 * 2*(order + 1) * 1e-6
xcenterpos = 0.033
xcentermom = 0.0
ycenterpos = 0.0
ycentermom = 0.00
tailfrac = 0
tailfac = 1
zlim = 90. * 227.92/360.
zmin = -zlim/2 # chopper duty 50
zmax = zlim/2
emean = 0.0800 #injection energy GeV, which can differ from the energy of synch particle
esigma = 0.000015
etrunc = 1 # flag for truncating the distribution
emin = emean-3*esigma
emax = emean+3*esigma

nparts = macroperturn
maxturns = injectionturns
xFunc = JohoTransverse(order, alphax, betax, emitxlim, xcenterpos, xcentermom, tailfrac, tailfac)
yFunc = JohoTransverse(order, alphay, betay, emitylim, ycenterpos, ycentermom, tailfrac, tailfac)
lFunc = GULongDist(zmin, zmax, syncPart, emean, esigma, etrunc, emin, emax)
injectionNode = TeapotInjectionNode(nparts,b,lostfoilbunch,foilparams,xFunc,yFunc,lFunc,maxturns)
addTeapotInjectionNode(lattice,0,injectionNode)

comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
rank = orbit_mpi.MPI_Comm_rank(comm)
#########################################
#Add dump node
#########################################
# The second parameter is the interval, e.g. dump the bunch to file every 5 turn
filename = "./output/dump.bunch"
utils.check_folder(filename) #check if the folder exists
dump = TeapotDumpNode(filename,100)
# add the dump node in a drift, the second parameter is postion
addTeapotDiagnosticsNode(lattice,0.001,dump)

# Note that the data is appended to the file, so don't use an exist file
stats = TeapotStatLatsNode('./output/stat.dat')
addTeapotDiagnosticsNode(lattice,0.0010001,stats)

##########################################
#Begin tracking
##########################################
starttime = time.time()
trackturn=2000
if isinstance(lattice,time_dep_csns.TIME_DEP_Lattice):
    lattice.setShowInfo(True)
    lattice.setTurns(trackturn) # Tracking for AC mode 
    lattice.trackBunchTurns(b,paramDict)
else:
    for i in xrange(trackturn):
        lattice.trackBunch(b,paramDict) # Tracking for DC mode
        bsize=b.getSizeGlobal()
        if rank==0:
            now = time.time()
            cost = now-starttime
            remain = timecalc(cost,macroperturn,i+1,injectionturns,trackturn)
            #print "Track on turn %d, cost %1.4f s,time remain %1.4f s, particles remain %d"%((i+1),cost,remain,bsize)

stats.closeFile()
endtime = time.time()
b.compress()
a=b.getSizeGlobal()
efturn = (injectionturns if (injectionturns < trackturn) else trackturn)
if rank==0:
    print "Total tracking time %1.4f"%(endtime-starttime)
    print "Injected particle size %d"%(macroperturn*efturn)
    print "Remain particle size %d"%a
