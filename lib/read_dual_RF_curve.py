from itertools import chain


def read_dual_RF_curve(filename):
    fd = open( filename, "r" )
    tempData = []
    time = []
    RFphase = []
    RF2phase = []
    RFV = []
    RF2V = []
    values = []
    while True:
        templine = fd.readline()
        if templine:
            tempData = templine.split("  ")
        else:
            break
    
        time.append(float(tempData[0])*1e-3)
        RFV.append(float(tempData[1]))
        RFphase.append(float(tempData[2]))
        RF2V.append(float(tempData[3]))
        RF2phase.append(float(tempData[4]))
        values.append(float(tempData[5]))
        tempData = []
    
    fd.close
    time = tuple(time)
    RFV = tuple(RFV)
    RFphase = tuple(RFphase)
    RF2V = tuple(RF2V)
    RF2phase = tuple(RF2phase)
    values = tuple(values)
    return time,RFV,RFphase,RF2V,RF2phase,values
