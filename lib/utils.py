import os
import sys

def check_folder(folder):
    '''check if the folder exists or is empty'''
    if os.path.exists(folder) and os.path.isdir(folder):
        print('Warning! The folder %s is not empty'% folder)
    else:
        os.makedirs(folder)

def confirm(question):
    '''Ask the user to confirm their action'''
    y = 'y'
    n = 'n'
    no = 'no'
    yes = 'yes'

    response = None
    while response not in ('y', 'n', 'yes', 'no'):
        response = input(question).lower()
    return response
