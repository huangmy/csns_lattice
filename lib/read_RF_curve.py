
from itertools import chain

def read_RF_curve(filename, multi=False):
    file_in = open(filename,"r")
    input_lines = file_in.readlines()
    file_in.close()
    #remove commented lines
    for i in input_lines:
        if ('!' in i) or ('#' in i):
            input_lines.remove(i)
    header = input_lines.pop(0)
    harms = [int(i) for i in header.split()]
    rfharms = harms[0]
    input_lines_split = map(lambda i: input_lines[i].replace('=','').replace(';','').split(), range(len(input_lines)))
    input_list = list(chain.from_iterable(input_lines_split))
    N = 2*rfharms+2
    RFVs = []
    RFphases = []
    time = [float(j) for j in input_list[::N]]
    time = tuple(time)
    values = [float(m) for m in input_list[1::N]]
    values = tuple(values)
    #read RFVoltages and RFPhases for multi-harmonics
    for ii in range(rfharms):
        RFV = [float(i) for i in input_list[2+ii*2::N]]
        RFVs.append(tuple(RFV))
        RFphase = [float(k) for k in input_list[3+ii*2::N]]
        RFphases.append(tuple(RFphase))
    if multi:
        return time,values,RFVs,RFphases,harms
    else:
        return time, values, RFVs[0], RFphases[0], harms[1]
