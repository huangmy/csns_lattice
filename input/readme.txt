s file is a description of the file rfdata*.multi / rfdat.dual
XXX.multi will be used to simulate the multi harmonic RFNode situation while the XXX.dual files only used to simulate the single or dual harmonic RFNode cases.
time(s) [SynchPhase or Brho] RFVoltage(GV) RFPhase(deg) RFVoltage(2) RFPhase(2) ......
rfdata.multi  is the case RFPhase synchronize with Brho[or synchEnergy], and the second column is Brho
rfdata1.multi is the case RFPhase is zero (treat as phase errors of the rf cavity), and the second column is Brho
rfdata2.multi is the case RFPhase synchronize with Brho[or synchEnergy], and the last second is SynchPhase
rfdata3.multi is the case RFPhase is zero (treat as phase errors of the rf cavity), and the second column is SynchPhase
rfdataFor300MeV.dual is the case that using dual_harmonic_cav Node. From left to right, each columm means "time(s) RFVoltage(GV) RFPhase(deg) RatioVoltage RFPhase2 Brho". Synctron Phase can be calcuated by program automatically, please keep the RFPhase being 0. You can also keep the RatioVoltage 0 to simulate the single harmonic RFNode cases.    
